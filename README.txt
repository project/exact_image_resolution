CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Exact image resolution module extends the image module giving the option to
input exact image sizes, if an image uploaded is outside that dimension, it
will be rejected.

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/exact_image_resolution


REQUIREMENTS
------------

This module requires the following modules:

 * Image (included in core)
 * File (included in core)


INSTALLATION
------------

 * Place the entire exact_image_resolution directory into sites modules
   directory (eg sites/all/modules).

 * Enable this module by navigating to:
    Administration > Modules

 * Install the module.

 * Go to admin/structure page. Click on manage fields of any content type.

 * Any field using the "Image" Field type will now have the added Exact image
   resolution option added to it when editing the field.

 * When using this option, it would be best to remove any inputs in the
   "Maximum image resolution", and "Minimum image resolution".


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


MAINTAINERS
-----------

Original Author: Gabriel Wong
Current Maintainers: Gabriel Wong
Email: gabrielkpwong@gmail.com
